package org.mymvc.test.service;

import org.mymvc.test.domain.Person;
import org.mymvc.test.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonService {
	
	@Autowired
	private PersonRepository personRepository;
	
	public Person getPersonById(final String id) {
		return personRepository.findOne(id);
	}
	
	public Person getPersonByName(final String name) {
		return personRepository.findByName(name); 
	}
	
	public Person getPersonByDepartment(final String department) {
		return personRepository.findByDepartment(department);
	}
}

package org.mymvc.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@EnableJpaRepositories("org.mymvc.test.repository")
@ComponentScan(value = "org.mymvc.test")
public class ContextRootConfig {
	
	 @Bean
	 public LocalEntityManagerFactoryBean entityManagerFactory() {
		 final LocalEntityManagerFactoryBean localContainerEntityManagerFactoryBean = 
				 new LocalEntityManagerFactoryBean();
		 localContainerEntityManagerFactoryBean.setPersistenceUnitName("spring-jpa-test");
		 localContainerEntityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter());
		 return localContainerEntityManagerFactoryBean;
	 }
	 
	 @Bean
	 public JpaVendorAdapter jpaVendorAdapter() {
		 EclipseLinkJpaVendorAdapter eclipseLinkJpaVendorAdapter = new EclipseLinkJpaVendorAdapter();
		 eclipseLinkJpaVendorAdapter.setDatabasePlatform("org.eclipse.persistence.platform.database.OraclePlatform");  
		 eclipseLinkJpaVendorAdapter.setGenerateDdl(false);
		 eclipseLinkJpaVendorAdapter.setShowSql(true);
		 return eclipseLinkJpaVendorAdapter;
	 }
	 
	 @Bean 
	 public JpaTransactionManager transactionManager() {
		 JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
		 jpaTransactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		 return jpaTransactionManager;
	 }
}
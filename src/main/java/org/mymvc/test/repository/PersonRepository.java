package org.mymvc.test.repository;

import org.mymvc.test.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

// all spring data jpa repos are transactional, will be scanned by spring, see Config.
public interface PersonRepository extends JpaRepository<Person, String> {
	
	/**
	 * Spring data-jpa code generated method for finding
	 * @param name
	 * @return
	 */
	public Person findByName(final String name);
	
	@Query("select p from Person p where p.department = :department")
	public Person findByDepartment(@Param("department") String department);
}

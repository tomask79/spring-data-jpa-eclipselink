package org.mymvc.test;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.mymvc.test.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
public class SpringMVCController {
	
	@Autowired
	private PersonService personService;
	
	@RequestMapping(value = "/personById/{personId}", method=RequestMethod.GET)
	public String getPersonById(@PathVariable("personId") String personId) {
		return "GetPerson By Id, Mr "+personService.getPersonById(personId); 
	}
	
	@RequestMapping(value = "/personByName/{personName}", method=RequestMethod.GET)
	public String getPersonByName(@PathVariable("personName") String personName) {
		return "GetPerson By Name, Mr "+personService.getPersonByName(personName);
	}
	
	@RequestMapping(value = "/personByDepartment/{department}", method=RequestMethod.GET)
	public String getPersonByDepartment(@PathVariable("department") String department) {
		return "GetPerson By Department, Mr "+personService.getPersonByDepartment(department);
	}
}

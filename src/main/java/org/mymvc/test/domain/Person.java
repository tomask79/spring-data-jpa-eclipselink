package org.mymvc.test.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Table(name="FRM_PERSON")
@Data
public class Person {
	@Id
	protected String id;
	
	@Column(name="name")
	protected String name;
	
	@Column(name="birthday_dt")
	@Temporal(TemporalType.DATE)
	protected Date birthday;
	
	@Column(name="department")
	protected String department;
}
